# bchidentity

bchidentity is a JavaScript library that lets you authenticate using the
[BCH Identity Protocol](identity_protocol.md).

## Example


```
const bchidentity = require("bchidentity");
(async () => {

    // Private key to sign authentication challenge with
    const wif = 'L4vmKsStbQaCvaKPnCzdRArZgdAxTqVx8vjMGLW5nHtWdRguiRi1';

    // The bchidentity login offer
    const offer = "bchidentity://example.org:443/login?op=login&chal=ci7P10BiPlL7B1QWa&cookie=fBAMYnHSwa";

    bchidentity.identify(offer, wif)
        .then(([status, response]) => {
            console.log("OK: ", status, response);
        })
        .catch(([status, response]) => {
            console.log("Error: ", status, response);
        });
})();
```

## Documentation

See [API reference](https://bitcoinunlimited.gitlab.io/bchidentity-js) for
complete overview of functions and parameters.

## License

This project is licensed under the MIT License.
