import * as id from "./bchidentity";

describe("parse_offer tests", () => {

    it("test_chal", () => {
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.chal).toEqual("a");

        // missing challenge throws
        expect(() => {
            id.parse_offer("bchidentity://hello.world/?op=login&cookie=b");
        }).toThrow(Error);

        // .. unless strict is disabled
        const b = id.parse_offer("bchidentity://hello.world/?op=login&cookie=b", false);
        expect(b.chal).toEqual(null);
    });

    it("test_cookie", () => {
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.cookie).toEqual("b");

        // missing cookie throws
        expect(() => {
            id.parse_offer("bchidentity://hello.world/?op=login&chal=a");
        }).toThrow(Error);

        // .. unless strict is disabled
        const b = id.parse_offer("bchidentity://hello.world/?op=login&chal=a", false);
        expect(b.cookie).toEqual(null);
    });

    it("test_domain", () => {
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.domain).toEqual("hello.world");

        // missing domain throws
        expect(() => {
            id.parse_offer("bchidentity:///?op=login&chal=a&cookie=b");
        }).toThrow(Error);

        // missing domain does not throw if strict is disabled
        const b = id.parse_offer("bchidentity:///?op=login&chal=a&cookie=b", false);
        expect(b.domain).toEqual("");
    });

    it("test_op", () => {
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.op).toEqual("login");

        // missing op throws
        expect(() => {
            id.parse_offer("bchidentity://hello.world/?chal=a&cookie=b");
        }).toThrow(Error);

        // .. unless strict is disabled
        const b = id.parse_offer("bchidentity://hello.world/?chal=a&cookie=b", false);
        expect(b.op).toEqual(null);
    });

    it("test_path", () => {
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.path).toEqual("/");

        const b = id.parse_offer(
            "bchidentity://hello.world/a/path?op=login&chal=a&cookie=b");
        expect(b.path).toEqual("/a/path");

        const c = id.parse_offer("bchidentity://nopath", false);
        expect(c.path).toEqual(null);
    });

    it("test_port", () => {
        // port defaults to 80
        const a = id.parse_offer(
            "bchidentity://hello.world/?op=login&chal=a&cookie=b");
        expect(a.port).toEqual(80);

        const b = id.parse_offer(
            "bchidentity://hello.world:443/?op=login&chal=a&cookie=b");
        expect(b.port).toEqual(443);
    });

    it("test_protocol", () => {
        // Wrong protocol throws
        expect(() => {
            id.parse_offer("wrong://hello.world/?op=login&chal=a&cookie=b");
        }).toThrow(Error);

        // No protocol throws
        expect(() => {
            id.parse_offer("hello.world/?op=login&chal=a&cookie=b");
        }).toThrow(Error);

        // Wrong protocol doesn ot throw when strict is disabled.
        expect(() => {
            id.parse_offer("wrong://hello.world/?op=login&chal=a&cookie=b", false);
        }).not.toThrow(Error);
        expect(() => {
            id.parse_offer("hello.world/?op=login&chal=a&cookie=b", false);
        }).not.toThrow(Error);
    });
});
